<?php 
namespace app;

include __DIR__.'/../vendor/autoload.php';
use app\files;
use PDO;
use PDOException;

class database extends files{
    private $type;
    private $login;
    private $adress;
    private $passwd="";
    private $name;
    private $data_connect;
    function __construct()
    {
        $get_as_string=$this->getfile(__DIR__."/../",".configuration");
        $help_array=explode(';',$get_as_string);
        for($i=0;$i<count($help_array);$i++){
            
            $helper_array=explode("=",$help_array[$i]);
            if("DATABASE_TYPE"== trim($helper_array[0])){
                $this->type=$helper_array[1];
            }
            if("DATABASE_SERVER"==trim($helper_array[0])){
                $this->adress=$helper_array[1];
            }
            if("DATABASE_NAME"==trim($helper_array[0])){
                $this->name=$helper_array[1];
            }
            if("DATABASE_LOGIN"==trim($helper_array[0])){
                $this->login=$helper_array[1];
            }
            if("DATABASE_PASSWD"==trim($helper_array[0])){
                $this->passwd=$helper_array[1];
            }
        }
        try{
            if($this->type=='sqlite'){
                $this->data_connect= new PDO("{$this->type}:".__DIR__."/baza.sql");
            }else{
                $this->data_connect= new PDO("{$this->type}:host={$this->adress};dbname={$this->name}",$this->login,$this->passwd);
            }
        }
        catch(PDOException $error){
            echo "Can't connect to database";
        }
     
    }
    public function create_table(string $name, array $kolumns ,bool $autoid=true){
        if($autoid){
            $kolumny="id INT AUTO_INCREMENT PRIMARY KEY, ";
        }
        else{
            $kolumny="";
        }
        for($i=0;$i<count($kolumns);$i++){
            if($i==count($kolumns)-1){
                $kolumny=$kolumny.implode(' ',$kolumns[$i]);
            }
            else{
                $kolumny=$kolumny.implode(' ',$kolumns[$i]).', ';
            }
            

        }
        
        $sql="CREATE table $name($kolumny);";
        try{
            $this->data_connect->exec($sql);
        }catch(PDOException $e){
            print_r($e);
        }

    }
    public function insert_to_table(string $name,array $values,array $colums){
        $questions="";
       for($i = 0;$i<count($colums);$i++){
            if($i==count($colums)-1){
                $questions=$questions.'?';
            }else{
            $questions=$questions."?,";
            }
        }
		$prepares=$this->data_connect->prepare('INSERT INTO '.$name.'('.implode(',',$colums).') VALUES ('.$questions.');');
        foreach($values as $value ){
        
           for($j=1;$j<=count($value);$j++){
               $prepares->bindValue($j,$value[$j-1]);
           }
           try{
               
            $prepares->execute();
           }
           catch(PDOException $e){
              echo 'Error don\'t insert data to table';
           }
        }
		
    }
    public function get_title(): string {
        $statement=$this->data_connect->prepare('SELECT title FROM Web_data LIMIT 1');
        $statement->execute();
        $result=$statement->fetch(PDO::FETCH_ASSOC);
       return $result['title'];


    }
    public function get_description() : string {
        $statement=$this->data_connect->prepare('SELECT description FROM Web_data LIMIT 1');
        $statement->execute();
        $result=$statement->fetch(PDO::FETCH_ASSOC);
       return $result['description'];
    }
    public function get_keyword(){
        $statement=$this->data_connect->prepare('SELECT keyword FROM Web_data LIMIT 1');
        $statement->execute();
        $result=$statement->fetch(PDO::FETCH_ASSOC);
       return $result['keyword'];
    }
    public function get_theme(){
        $statement=$this->data_connect->prepare('SELECT theme FROM Web_data LIMIT 1');
        $statement->execute();
        $result=$statement->fetch(PDO::FETCH_ASSOC);
       return $result['theme'];
    }
}