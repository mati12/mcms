<?php
 namespace app;
 include __DIR__.'/../vendor/autoload.php';
 use Thedudeguy\Rcon;
 use app\files;
use Exception;

class rcons extends files{
    private $conect_rcon; 
    private $host;
    private $port;
    private $passwd="";
    private $timeout=6;

    function __construct()
    {
        $get_as_string=$this->getfile(__DIR__."/../",".configuration");
        $help_array=explode(';',$get_as_string);

        for($i=0;$i<count($help_array);$i++){
            
            $helper_array=explode("=",$help_array[$i]);
            if("RCON_SERVER"== trim($helper_array[0])){
                $this->host=$helper_array[1];
            }
            if("RCON_PORT"==trim($helper_array[0])){
                $this->port=$helper_array[1];
            }
            if("RCON_PASSWD"==trim($helper_array[0])){
                $this->passwd=$helper_array[1];
            }
        }
        try{
           $this->conect_rcon=new Rcon($this->host,$this->port,$this->passwd,$this->timeout);
           if(!$this->conect_rcon->connect()){
               throw new Exception("Can't connect to minecraft server");
           }
        }
        catch(Exception $error){
            return $error->getMessage();
        
        }
     
    }
    public function custom_rcon_comands(string $comand){
        if($this->conect_rcon->sendCommand($comand)){
            return "Done";
        }
        else{
            return "Can't send comand";
        }
    }
    public function get_message(){
        return $this->conect_rcon->getResponse();
    }
   
 }