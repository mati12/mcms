<? namespace app;

include __DIR__.'/../vendor/autoload.php';
use app\files;
class mail extends files{
    private $server;
    private $port;
    private $username;
    private $passwd="";
    private $mailer;

    function __construct()
    {
        $get_as_string=$this->getfile(__DIR__."/../",".configuration");
        $help_array=explode(';',$get_as_string);

        for($i=0;$i<count($help_array);$i++){
            
            $helper_array=explode("=",$help_array[$i]);
            if("EMAIL_SERVER"== trim($helper_array[0])){
                $this->server=$helper_array[1];
            }
            if("EMAIL_PORT"==trim($helper_array[0])){
                $this->port=$helper_array[1];
            }
            if("EMAIL_LOGIN"==trim($helper_array[0])){
                $this->username=$helper_array[1];
            }
            if("EMAIL_PASSWD"==trim($helper_array[0])){
                $this->passwd=$helper_array[1];
            }
        }
        $trans=(new Swift_SmtpTransport($this->server, $this->port))->setUsername($this->username)->setPassword($this->passwd);
        $this->mailer=new Swift_Mailer($trans);

    }
    public function send_text_email(string $text){
        
    }

}
?>