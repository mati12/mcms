<?php
require 'vendor/autoload.php';
use app\database;
use app\files;
use app\rcons;
use Thedudeguy\Rcon;

chmod(__DIR__.'/assets',0777);
chmod(__DIR__.'/themes',0777);
if(!file_exists('.configuration')){
header('Location:./install');
}
if(!file_exists('.htaccess')){
    file_put_contents('.htaccess','Options -MultiViews
    RewriteEngine On

    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    
    RewriteRule ^(.+)$ index.php?url=$1 [QSA,L]');
 }

 function parseUrl(){
    if(isset($_GET['url'])){
    return $url = explode("/", filter_var(rtrim($_GET["url"], "/"), FILTER_SANITIZE_URL));
    
}
}
$url=parseUrl();
 function show($page='/',$id=0){
    $files=new files();
    $database=new database();
    $rcon=new rcons();
    $favico=$files->getFavico();
    $title=$database->get_title();
    $description=$database->get_description();
    $keywords=$database->get_keyword();
    $theme=$database->get_theme();
    if(file_exists(__DIR__."/themes/".$theme."/func.php")){
        include __DIR__."/themes/".$theme."/func.php";
    }
    include __DIR__."/themes/".$theme."/header.php";
    try {
        if($page=="/"||$page==""){
           if(!include __DIR__."/themes/".$theme."/home.php"){
               throw new Exception("Brak strony",404);
           }
        }
        else{
            if(!include __DIR__."/themes/".$theme."/".$page.".php"){
                throw new Exception("Page not exist",404);
            }
        }
    } catch (Exception $errors) {
       if($errors->getCode()==404){
           http_response_code(404);
           include(__DIR__."/themes/".$theme."/404.php");
       }
    }
    include __DIR__."/themes/".$theme."/footer.php";
 };
show($url[0],$url[1]);
?>