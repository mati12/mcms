<?php
session_start();

if(!isset($_SESSION['install']) && $_SESSION['install']==true){
    header('Location:/');
    }
?>
<!DOCTYPE html>
<Head>
<title>Configure your Website</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<link rel="stylesheet" href="src/custom.css" type="text/css"/>
</Head>
<body>
<div class="container">
<div class="d-flex col-sm-8 mx-auto contener" >
<div class="p-5 installer w-100 justify-content-center">
<div class="text-center">
<H1>Configure your website</H1>
<form method="POST" action="set_info_controller.php" enctype="multipart/form-data">
  <label >Name your website <span class="text-danger">*</span></label><br>
    <input type="text" name="Title" required><br>
  
    <label>Descrypt your website <span class="text-danger">*</span></label><br>
    <input type="text" name="description" required><br>
    <label>Tags your website(separate with commas) <span class="text-danger">*</span></label><br>
    <input type="text" name="tags" required><br>
    <label>Choose your favico</label><br>
    <input type="file" name="favico" accept="image/*"><br>
    <label>Enter admin login</label> <span class="text-danger">*</span><br>
    <input type="text" name="login" required><br>
    <label>Enter admin email <span class="text-danger">*</span></label><br>
    <input type="email" name="email" required><br>
    <label>Enter admin password <span class="text-danger">*</span></label><br>
    <input type="password" name="passwd"required><br>
    <input type="submit" name="Finish" class="my-3" value="Finish"></input>
</form>
</div>
</div>
</div>
</div>
</body>