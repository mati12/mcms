<?php
session_start();
if(file_exists('../.configuration')){
header('Location:/');
}
if(!file_exists('.htaccess')){
    file_put_contents('.htaccess','Options -Indexes');
 }
$_SESSION['install']=true;
?>
<!DOCTYPE html>
<Head>
<title>Welcome installer MCMS</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<link rel="stylesheet" href="src/custom.css" type="text/css"/>
</Head>
<body>
<div class="container">
<div class="d-flex col-sm-8 mx-auto contener" >
<div class="p-5 installer w-100 justify-content-center">
<div class="welcome text-center">
    <H1>Welcome MCMS Installer</H1>
    <p class="text-center">
        Welcome to the Convenient Content Management System installer for your Minecraft server site.
    </p>
    <button class="float-right" id="lets_go">Let's Go</button>
</div>
<div class="database text-center d-none">
    <h1>Config your database</h1>
    <form method="POST">
        <select name="bazy_danych" id="database-select">
            <option value="sqlite">sqlite</option>
            <option value="mysql">Mysql/Mariadb</option>
            <option value="pgsql">Postgrsql</option>
        </select>
        <div class="d-none" id='baza'>
            <label>Enter the database server address <span class="text-danger">*</span></label></br>
            <input type="text" class="datainput" id="server"></br>
            <label>Enter a database name <span class="text-danger">*</span></label></br>
            <input type="text" class="datainput"  id="database" ></br>
            <label>Enter the login to the database <span class="text-danger">*</span></label></br>
            <input type="text" class="datainput"  id="login" ></br>
            <label>Enter the database password</label></br>
            <input type="text" class="datainput" id="passwd"></br>
        </div>
        <Button type="button" class="my-3" id="next">
            Next
        </Button>
    </form>
</div>
<div class="email text-center d-none">
    <H1>Config E-mail data</H1>
    <form method="POST">
       <input type="checkbox" id="if_email_setting">
       <label>The site will send e-mails. </label><br>
        <div class="d-none" id='mailserver'>
            <label>Enter the mail server <span class="text-danger">*</span></label></br>
            <input type="text" class="emailinput" id="server"  placeholder="smtp.gmail.com"></br>
            <label>Enter a mail server port <span class="text-danger">*</span></label></br>
            <input type="number" class="emailinput"  id="serverport" placeholder="465" ></br>
            <label>Enter the login to mail server <span class="text-danger">*</span></label></br>
            <input type="text" class="emailinput"  id="login" placeholder="example@gmail.com"></br>
            <label>Enter the mail server password <span class="text-danger">*</span></label></br>
            <input type="text" class="emailinput" id="passwd" placeholder="your password"></br>
        </div>
        <Button type="button" class="my-3" id="nextmail">
            Next
        </Button>
    </form>
</div>
<div class="rcon text-center d-none">
    <H1>Config Rcon data</H1>
    <form method="POST" action="set_config.php" id="finish_config">
       <input type="checkbox" id="if_rcon_setting">
       <label>The site will connect to minecraft server</label><br>
        <div class="d-none" id='rconserver'>
            <label>Enter the adress minecraft rcon console <span class="text-danger">*</span></label></br>
            <input type="text" class="rconinput" id="server"  placeholder="examminecraft.com"></br>
            <label>Enter a rcon console port <span class="text-danger">*</span></label></br>
            <input type="number" class="rconinput"  id="serverport" placeholder="25576" ></br>
            <label>Enter the password to rcon console</label></br>
            <input type="text" class="rconinput"  id="passwd" placeholder="ghbtgvuyjhuu" ></br>
        </div>
        <Button type="button" class="my-3" id="nextrcon">
            Next
        </Button>
    </form>
</div>
</div>
</div>
</div>
<script>
    const clickStart=document.querySelector('#lets_go');
    const database=document.querySelector('.database');
    const email = document.querySelector('.email');
    const rcon = document.querySelector('.rcon');
    clickStart.addEventListener("click",()=>{
        const welcome = document.querySelector('.welcome');
        const database=document.querySelector('.database');
        welcome.classList.toggle('d-none');
        database.classList.toggle('d-none');
    })
    const databaseSelect=document.querySelector('#database-select');
    databaseSelect.addEventListener("change",()=>{
         const baza=document.querySelector('#baza');
        if(databaseSelect.options[databaseSelect.selectedIndex].value != 'sqlite'){
           
            baza.classList.remove('d-none');
        }
        else{
            baza.classList.add('d-none');
        }
    });
    const next=document.querySelector('#next');
    next.addEventListener('click',()=>{
      
        const type = databaseSelect.options[databaseSelect.selectedIndex].value;
        let database_data = [type];
        if(type!='sqlite'){
            const input_data=document.querySelectorAll('.datainput');
            input_data.forEach(input=>{
                if(input.id!="passwd"){
                if(input.value.length!=0){
                database_data.push(input.value);
                }else{
                    alert('Database server, database and login can\'t be emty');
                }
                }else{
                    database_data.push(input.value);
                }

               
            });
            if(database_data.length>3){
                document.cookie="dane="+database_data;
                database.classList.add('d-none');
        email.classList.remove('d-none');
            }
        }
        else{
            document.cookie="dane="+database_data;
            database.classList.add('d-none');
        email.classList.remove('d-none');
        }
    });
    const next_email=document.querySelector('#nextmail');
    const email_check =document.querySelector('#if_email_setting');
    next_email.addEventListener("click",()=>{
        const emails=[];
        if(!email_check.checked){
            document.cookie="email="+["","","",""];
            email.classList.add('d-none');
            rcon.classList.remove('d-none');

        }else{
            const input_email=document.querySelectorAll('.emailinput');
            
            input_email.forEach(inpute=>{
               emails.push(inpute.value);
               
            });
            document.cookie="email="+emails;
            email.classList.add('d-none');
            rcon.classList.remove('d-none');
        }
    })
    
    email_check.addEventListener("change",()=>{
        const email_form = document.querySelector('#mailserver')
        if(email_check.checked){
            email_form.classList.remove('d-none');


        }else{
            email_form.classList.add('d-none');
        }
    });
    const rcon_finish=document.querySelector('#nextrcon');
    const rcon_check =document.querySelector('#if_rcon_setting');
    rcon_finish.addEventListener("click",()=>{
        const rcons=[];
        if(!rcon_check.checked){
            document.cookie="rcon="+["","","",""];
            document.forms["finish_config"].submit();

        }else{
            const input_rcon=document.querySelectorAll('.rconinput');
            
            input_rcon.forEach(inputr=>{
               rcons.push(inputr.value);
               
            });
            document.cookie="rcon="+rcons;
            document.forms["finish_config"].submit();

        }
    })
    
    rcon_check.addEventListener("change",()=>{
        const rcon_form = document.querySelector('#rconserver')
        if(rcon_check.checked){
            rcon_form.classList.remove('d-none');


        }else{
            rcon_form.classList.add('d-none');
        }
    })

</script>
</body>
<html>