<?php
require '../vendor/autoload.php';
use app\database;
use app\files;
if(file_exists('../.configuration')){
    header('Location:/');
}


function set_database(){
$database=new database();
$database->create_table('Web_data',[['title','varchar(255)'],['description','text'],['keyword','text'],['theme','varchar(255)']]);
$database->create_table('Files',[['nazwa','varchar(255)'],['id_user','int']]);
$database->create_table('Photos',[['nazwa','varchar(255)'],['album','varchar(255)'],['id_user','int']]);
$database->create_table('Users',[['nick','varchar(255)'],['email','varchar(255)'],['passwd','char(60)'],['role','varchar(255)']]);
$database->create_table('Articles',[['Title','varchar(255)'],['article','text'],['date','datetime'],['id_user','int']]);
$database->create_table('menu',[['nazwa','varchar(255)'],['adres','varchar(255)']]);
header('Location:set_info.php');
};

if(isset($_COOKIE['dane'])&&isset($_COOKIE['email'])&&isset($_COOKIE['rcon'])){
$conf_controller=new files();
$settings=explode(',',$_COOKIE['dane'].','.$_COOKIE['email'].','.$_COOKIE['rcon']);
$filecontent="DATABASE_TYPE={$settings[0]};
DATABASE_SERVER={$settings[1]};
DATABASE_NAME={$settings[2]};
DATABASE_LOGIN={$settings[3]};
DATABASE_PASSWD={$settings[4]};
EMAIL_SERVER={$settings[5]};
EMAIL_PORT={$settings[6]};
EMAIL_LOGIN={$settings[7]};
EMAIL_PASSWD={$settings[8]};
RCON_SERVER={$settings[9]};
RCON_PORT={$settings[10]};
RCON_PASSWD={$settings[11]};";
$conf_controller->genfile("../",".configuration",$filecontent);
set_database();
unset($_COOKIE['dane']);
unset($_COOKIE['email']);
unset($_COOKIE['rcon']);
}