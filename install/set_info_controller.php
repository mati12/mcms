<?php
require '../vendor/autoload.php';
use app\database;
use app\files;
if(isset($_FILES)){
    $favico=new files();
    $favico->putFavico($_FILES['favico']);
}
if(isset($_POST)){
    if(!isset($_POST['Title'])||!isset($_POST['description'])||!isset($_POST['tags'])){
        echo "<script>
alert('Enter required field');
window.location.href='set_info.php';
</script>";
    }
    elseif(!isset($_POST['login'])||!isset($_POST['passwd'])||!isset($_POST['email'])){
        echo "<script>
        alert('Enter required field');
        window.location.href='set_info.php';
        </script>"; 
    }
    elseif(!filter_var($_POST['email'],FILTER_SANITIZE_EMAIL)){
        echo "<script>
        alert('Enter good email');
        window.location.href='set_info.php';
        </script>"; 
    }
    else{

    $database=new database();
    $database->insert_to_table('Web_data',[[$_POST['Title'],$_POST['description'],$_POST['tags'],'default']],['title','description','keyword','theme']);
    $database->insert_to_table('Users',[[$_POST['login'],$_POST['email'],password_hash($_POST['passwd'],PASSWORD_BCRYPT),'admin']],['nick','email','passwd','role']);
    }
}